import Link from "next/link";
import { Section } from "../shared/Section";
import { Animate } from "../shared/animations";
import { AnimateText } from "../shared/animations/AnimateText";
import { RiPulseLine } from "react-icons/ri";

const CallToActionPrimary = () => {
	return (
		<Section className="pt-32 pb-16">
			<div>
				<div className="mb-10">
					<AnimateText
						text="Maximize efficiency"
						className="text-[8rem] leading-[1.1] font-medium mb-5 text-left"
						styleIndex={{
							index: 1,
							className: "text-lightQuaternary",
						}}
					/>
					<AnimateText
						text="with our intuitive"
						className="text-[8rem] leading-[1.1] font-medium mb-5 text-left"
					/>
				</div>

				<div className="flex justify-between">
					<div className="flex items-center">
						<Animate
							once={false}
							animate={{
								hidden: {
									scale: 0,
								},
								visible: {
									scale: 1,
									transition: {
										duration: 0.5,
										ease: "easeInOut",
									},
								},
							}}
						>
							<div className="flex justify-center items-center relative w-48 h-48 bg-lightTertiary rounded-full -mr-5 z-10">
								<RiPulseLine
									size="3rem"
									className="fill-white bg-orange rounded-xl p-2 relative z-10"
								/>
								<div className="absolute w-[70%] h-[1px] border-t-2 border-dotted border-orange/70 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2"></div>
							</div>
						</Animate>
						<Animate
							once={false}
							animate={{
								hidden: {
									scale: 0.5,
								},
								visible: {
									scale: 1,
									transition: {
										duration: 0.5,
										ease: "easeInOut",
									},
								},
							}}
						>
							<div className="flex flex-col justify-center items-center relative w-48 h-48 bg-yellow rounded-full ">
								<h4 className="text-3xl font-semibold">+30%</h4>
								<p className="w-[60%] text-center text-sm">
									Speed up your productivity
								</p>
							</div>
						</Animate>
					</div>
					<Animate
						once={false}
						animate={{
							hidden: {
								scale: 0.2,
							},
							visible: {
								scale: 1,
								transition: {
									duration: 1,
									ease: "easeOut",
									type: "spring",
								},
							},
						}}
					>
						<div className="w-[500px] overflow-hidden rounded-[5rem] py-1 bg-yellow h-fit">
							<h1 className="text-[8rem] font-medium text-left text-nowrap marquee">
								analytics service
							</h1>
						</div>
					</Animate>
				</div>
				<div className="flex justify-between py-14 mt-16 border-t">
					<Animate
						className="overflow-hidden py-2"
						once={false}
						animate={{
							hidden: {
								y: 100,
								opacity: 0,
							},
							visible: {
								y: 0,
								opacity: 1,
								transition: {
									duration: 0.5,
									ease: "easeInOut",
								},
							},
						}}
					>
						<p className="text-lg max-w-2xl leading-6">
							Explore traffic sources, page behaviour, conversions and more to
							gain deep insight into your audience. With us, your business
							doesn&apos;t just adapt - it evolves
						</p>
					</Animate>
					<Animate
						once={false}
						animate={{
							hidden: {
								scale: 0.3,
							},
							visible: {
								scale: 1,
								transition: {
									duration: 1,
									ease: "easeOut",
									type: "spring",
								},
							},
						}}
					>
						<div className="flex gap-4">
							<Link
								href={"#"}
								className="inline-block rounded-2xl px-10 py-4 text-sm font-normal  bg-lightTertiary text-dark"
							>
								Request a demo
							</Link>
							<Link
								href={"#"}
								className=" inline-block rounded-2xl px-10 py-4 text-sm font-normal  bg-orange text-white"
							>
								Start for free
							</Link>
						</div>
					</Animate>
				</div>
			</div>
		</Section>
	);
};

export default CallToActionPrimary;
