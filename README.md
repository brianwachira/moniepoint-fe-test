# Moniepoint Frontend Test
This is a test curated to convert a [Dribbble design](https://cdn.dribbble.com/userupload/12362584/file/original-634f43609200b21dab651386524d30ac.mp4) into a working web application.

## Demo Link
- Link : [https://m-f-t.vercel.app/](https://m-f-t.vercel.app/)
- GIF : [https://gitlab.com/brianwachira/moniepoint-fe-test/-/blob/main/public/assets/images/screen-capture.gif](https://gitlab.com/brianwachira/moniepoint-fe-test/-/blob/main/public/assets/images/screen-capture.gif)


## Tech Stack ( Frameworks & major libraries)
- NextJS - [https://nextjs.org/docs](https://nextjs.org/docs)
- TailwindCSS - [https://tailwindcss.com/](https://tailwindcss.com/)
- Framer Motion - [https://www.framer.com/motion/](https://www.framer.com/motion/)
- React Icons - [https://react-icons.github.io/react-icons/](https://react-icons.github.io/react-icons/)
- React Intersection Observer - [https://react-intersection-observer.vercel.app/?path=/docs/intro--docs](https://react-intersection-observer.vercel.app/?path=/docs/intro--docs)
- Vercel - [https://vercel.com/dashboard](https://vercel.com/dashboard)


## Project structure
```
src 
└───components
│   └───navigation
│       │   Header.tsx
│       │   Footer.tsx
│   └───sections
│       │   Example.tsx
│   └───shared
│       └───example3
│           │   Example4.tsx
│       │   Example.tsx
|
|
└───utils
│   └───example.ts
|
└───Pages
│   └───Example1
│       │   index.tsx
│   └───Example2.tsx
```

## Getting Started
- Install node modules:
```bash
npm install

```
- Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Known bugs
 - Tuning transitions on the section that has a mobile phone, [ProductShowcase](https://gitlab.com/brianwachira/moniepoint-fe-test/-/blob/main/src/components/sections/ProductShowcase.tsx?ref_type=heads)
 
## Support and contact details
Contact me on brianwachira7@gmail.com for any comments, reviews or advice.
