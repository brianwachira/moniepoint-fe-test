import { cn } from "@/utils/cn";
import React from "react";

type SectionProps = {
	children: React.ReactNode;
	className?: string;
} & React.ComponentPropsWithRef<"div">;

export const Section = React.forwardRef<HTMLDivElement, SectionProps>(
	({ children, className, ...props }, ref) => {
		return (
			<div
				{...props}
				ref={ref}
				className={cn(
					"container max-w-[1440px]  mx-auto px-4 sm:px-7 md:px-10 lg:px-14",
					className
				)}
			>
				{children}
			</div>
		);
	}
);

Section.displayName = "Section";
