import { useEffect, useState } from "react";
import { useInView } from "react-intersection-observer";
import { Section } from "../shared/Section";
import { AnimateText } from "../shared/animations/AnimateText";
import { Animate } from "../shared/animations";
import { TbChartHistogram } from "react-icons/tb";
import { ProgressBar, SegmentedProgressBar } from "../shared/ProgressBar";
import { dummyData } from "@/constants";
import Image from "next/image";
import {
	HiOutlineDocumentReport,
	HiOutlinePresentationChartLine,
} from "react-icons/hi";
import { GoGitCompare } from "react-icons/go";

const DataOverview = () => {
	const [progress, setProgress] = useState(0);
	const [slideUpClass, setSlideUpClass] = useState("");
	const [ref, refInView] = useInView({});
	const [ref2, refInView2] = useInView({
		triggerOnce: true,
		threshold: 0.5,
		delay: 500,
	});
	useEffect(() => {
		if (refInView) {
			setProgress(100);
		} else {
			setProgress(0);
		}
	}, [refInView]);
	useEffect(() => {
		if (refInView2) {
			setSlideUpClass("slide-up");
		} else {
			setSlideUpClass("");
		}
	}, [refInView2]);

	const reports = [
		{
			title: "Sales reports",
			profit: "$ 264,2K",
			chart: "/assets/images/ramos-bar-chart.png",
			icon: HiOutlineDocumentReport,
		},
		{
			title: "Finance reports",
			profit: "$ 264,2K",
			chart: "/assets/images/ramos-bar-chart.png",
			icon: GoGitCompare,
		},
		{
			title: "Insights",
			profit: "$ 264,2K",
			chart: "/assets/images/ramos-bar-chart.png",
			icon: HiOutlinePresentationChartLine,
		},
	];

	return (
		<Section className="pt-16 pb-16">
			<AnimateText
				className="text-[8rem] leading-[1.1] font-medium mt-5 text-left text-balance py-2"
				text="We give you full"
			/>
			<AnimateText
				text="control over your data"
				styleIndex={{
					index: 0,
					className: "text-lightQuaternary",
				}}
				className="text-[8rem] leading-[1.1] font-medium mt-5 text-left text-balance"
			/>
			<div className="grid grid-cols-2 gap-6 mt-24">
				<div className="rounded-[2rem] border bg-light p-14 relative">
					<div className="w-full h-3/5 absolute top-0 left-0 bg-repeat bg-center bg-contain bg-[url('/assets/images/ramos-squares-fig.png')]" />
					<div className="relative flex">
						<div className="bg-white rounded-3xl p-12 text-center w-[50%] space-y-5 shadow-2xl">
							<h5 className="text-lg font-medium">Conversion rate</h5>
							<div className="relative w-full p-5 rounded-3xl" ref={ref}>
								<Animate
									className="absolute -top-3 -left-3 z-[1]"
									once={false}
									animate={{
										hidden: {
											scale: 0.1,
										},
										visible: {
											scale: 1,
											transition: {
												duration: 2,
												ease: "easeInOut",
												type: "spring",
												bounce: 0.5,
											},
										},
									}}
								>
									<div className="relative z-[1] w-11 h-11 rounded-full bg-light  flex items-center justify-center shadow-md">
										<div className="bg-white rounded-full flex items-center justify-center w-8 h-8">
											<TbChartHistogram size="1rem" className="text-dark" />
										</div>
									</div>
								</Animate>
								<ProgressBar
									duration={2}
									className="absolute w-full h-full top-0 left-0 rounded-3xl"
									fillclassname="bg-yellow"
									progress={progress}
								/>
								<h3 className="text-6xl text-center font-medium relative z-[2]">
									2,3%
								</h3>
							</div>
							<p className="text-gray-500 text-balance text-lg font-light">
								Percentage of website visitors
							</p>
						</div>
						<Animate
							className="w-[50%]"
							once={false}
							animate={{
								hidden: {
									x: -20,
								},
								visible: {
									x: 0,
									transition: {
										duration: 2,
										ease: "easeInOut",
										type: "spring",
										bounce: 0.5,
									},
								},
							}}
						>
							<div className="bg-white border  rounded-3xl p-5 w-[100%] relative -ml-8 top-10 shadow-2xl flex flex-col">
								<h5 className="font-light text-gray-500">Sales revenue</h5>
								<h3 className="text-3xl font-base text-balance my-3">
									<span className="text-xl font-normal">$</span> 131,2K
								</h3>
								<div className="my-3" ref={ref}>
									<SegmentedProgressBar progress={progress} />
								</div>
								<div className="my-5 space-y-3">
									{dummyData.items.map((item, index) => (
										<div
											key={item.title}
											className="flex justify-between items-center"
										>
											<div className="flex gap-2 items-center">
												<Image
													src={item.image}
													width={35}
													height={35}
													alt="profile"
													className="rounded-full"
												/>
												<p className="text-sm text-gray-500 font-light">
													{item.title}
												</p>
											</div>
											<p className="font-medium">{item.value}</p>
										</div>
									))}
								</div>
								<div className="border-t  pt-3 flex justify-between w-full">
									<p className="text-gray-500 text-sm font-light">
										Engagement rate
									</p>
									<p className="font-medium">47.84%</p>
								</div>
							</div>
						</Animate>
					</div>
					<div className=" pt-14 px-8">
						<h4 className="text-center font-medium text-3xl mt-10">
							Improved customer service
						</h4>
						<p className="text-center text-gray-500 text-lg font-light mt-4">
							Analytics helps optimize service processes by<br/>providing
							information on how to improve interactions<br/>with customers and
							increase their satisfaction.
						</p>
					</div>
				</div>
				<div className="rounded-[2rem] border bg-light p-14 relative">
					<div className="w-full h-3/5 absolute top-0 left-0 bg-repeat bg-center bg-contain bg-[url('/assets/images/ramos-squares-fig.png')]" />
					<div
						ref={ref2}
						className="w-[90%] mx-auto flex flex-col relative cards-stacked-container"
					>
						{reports.map((item, index) => (
							<div
								key={index}
								className={`rounded-3xl bg-white p-5 shadow-lg border card-stacked flex justify-between gap-3 w-full ${
									index === 2 && slideUpClass === ""
										? "opacity-0"
										: slideUpClass === "slide-up" && index === 2
										? `${slideUpClass}`
										: index < 2
										? "opacity-100"
										: "opacity-0"
								}`}
							>
								<div className="flex flex-col ">
									<div className="flex items-center gap-2">
										<item.icon
											size="2rem"
											className="text-dark bg-light p-2 rounded-lg"
										/>
										<p className="font-light">{item.title}</p>
									</div>
									<div className="mt-10 flex-1 flex flex-col justify-end">
										<h5 className="text-gray-500 font-light text-sm">
											Total profit
										</h5>
										<h3 className="text-2xl font-medium text-balance mt-2">
											{item.profit}
										</h3>
										<button className="rounded-lg bg-yellow font-medium mt-5 text-dark text-xs px-3 py-2">
											Data visualization
										</button>
									</div>
								</div>
								<div className="flex justify-end">
									<Image
										src={item.chart}
										width={250}
										height={250}
										alt="ramos:chart"
									/>
								</div>
							</div>
						))}
					</div>
					<div className=" pt-5 px-14">
						<h4 className="text-center font-medium text-3xl mt-10">
							Monitoring key indicators
						</h4>
						<p className="text-center text-gray-500 text-lg font-light mt-4">
							Analytics platforms allow businesses to track <br/>KPIs, an important
							tool for measuring success <br/>and achieving goals.
						</p>
					</div>
				</div>
			</div>
		</Section>
	);
};

export default DataOverview;
