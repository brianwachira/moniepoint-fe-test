import { useEffect, useState } from "react";
import { useInView } from "react-intersection-observer";
import { Section } from "../shared/Section";
import { Animate } from "../shared/animations";
import { HiOutlineSquare3Stack3D } from "react-icons/hi2";
import { ProgressBar } from "../shared/ProgressBar";
import { AnimateCounter } from "../shared/animations/AnimateCounter";
import { BsFillArrowUpCircleFill } from "react-icons/bs";
import Image from "next/image";

const Analytics = () => {
	const [progress, setProgess] = useState(0);
	const [ref, refInView] = useInView({
		triggerOnce: false,
		delay: 500,
	});

	useEffect(() => {
		if (refInView) {
			setProgess(50);
		} else {
			setProgess(0);
		}
	}, [refInView]);

	return (
		<Section className="bg-light rounded-t-[100px] rounded-b-[100px] py-20 lg:px-20">
			<div className="flex justify-between items-center">
				<div className="w-[62%]">
					<Animate
						className="overflow-hidden py-2"
						animate={{
							hidden: {
								y: 100,
							},
							visible: {
								y: 0,
								transition: {
									type: "spring",
									duration: 1,
									ease: "easeInOut",
								},
							},
						}}
					>
						<h3 className="text-6xl font-medium text-balance mb-6">
							Your key to strategic success through analytics
						</h3>
					</Animate>
				</div>
				<div className="w-[33%] flex justify-end">
					<Animate
						className="overflow-hidden py-2"
						animate={{
							hidden: {
								y: 100,
								opacity: 0,
							},
							visible: {
								y: 0,
								opacity: 1,
								transition: {
									duration: 0.5,
									ease: "easeInOut",
								},
							},
						}}
					>
						<p className="text-balance text-2xl">
							Ready for exciting, instantaneous, all-accessible insights in real
							time?
						</p>
					</Animate>
				</div>
			</div>
			<div className="flex justify-between gap-5 mb-16" ref={ref}>
				<div className="w-[60%] rounded-[2.4rem] bg-white border border-gray-200 mt-10 flex shadow-2xl">
					<div className="p-12 w-6/12 flex flex-col">
						<Animate
							className=""
							animate={{
								hidden: {
									scale: 0.5,
								},
								visible: {
									scale: 1,
									transition: {
										duration: 0.3,
										ease: "linear",
									},
								},
							}}
						>
							<div className="bg-yellow w-fit shadow-xl text-dark  px-4 p-2 rounded-xl border border-1">
								Setting up reports
							</div>
						</Animate>
						<div className="flex-1 flex flex-col justify-end space-y-5">
							<h4 className="text-3xl font-medium">
								Fast and easy access to analytics
							</h4>
							<Animate
								delay={0.5}
								animate={{
									hidden: {
										opacity: 0.1,
										y: 10,
									},
									visible: {
										opacity: 1,
										y: 0,
										transition: {
											type: "spring",
											duration: 1,
											ease: "linear",
										},
									},
								}}
							>
								<p className="text-lg text-gray-400 font-normal">
									One platform is a comprehensive system of solutions that will
									be the first step towards digitization of your business!
								</p>
							</Animate>
						</div>
					</div>
					<div className="p-5 mt-10 border-t border-l rounded-tl-3xl border-gray-200 w-6/12">
						<h5 className="text-xl font-medium">Sales Statistics</h5>
						<Animate
							delay={0.5}
							animate={{
								hidden: {
									opacity: 0.1,
									y: 20,
								},
								visible: {
									opacity: 1,
									y: 0,
									transition: {
										duration: 0.5,
										ease: "linear",
									},
								},
							}}
							className="mt-2"
						>
							<div className="flex items-center gap-2">
								<div className="flex items-center gap-5 w-[60%]">
									<div className="flex items-center justify-center bg-orange rounded-full w-14 h-14">
										<HiOutlineSquare3Stack3D
											size="1.2rem"
											className="text-white"
										/>
									</div>

									<div>
										<h6 className="text-gray-400 font-medium text-base">
											Total profit
										</h6>
										<h3 className="text-3xl font-medium text-balance mt-1">
											<span className="text-lg font-normal">$</span> 264,2K
										</h3>
									</div>
								</div>
								<div className="bg-light rounded-2xl p-3 w-[40%]">
									<h6 className="font-medium text-base">Visitors</h6>
									<ProgressBar progress={progress} className="mt-2" />
									<h4 className="text-[2rem] font-base mt-2">
										<AnimateCounter value={56} />K
										<sup className="text-green text-[0.5rem] align-super ml-1">
											<BsFillArrowUpCircleFill
												size="1rem"
												className="inline mr-1"
											/>
											+14%
										</sup>
									</h4>
								</div>
							</div>
						</Animate>
						<Animate
							delay={0.5}
							animate={{
								hidden: {
									opacity: 0.1,
									y: 20,
								},
								visible: {
									opacity: 1,
									y: 0,
									transition: {
										delay: 0.1,
										duration: 0.4,
										ease: "linear",
									},
								},
							}}
							className="mt-5"
						>
								<Image
									src={"/assets/images/ramos-linechart.png"}
									width={500}
									height={500}
									alt="ramos:linechart"
								/>
						</Animate>
					</div>
				</div>
				<div className="w-[40%] rounded-[2.4rem] bg-dark border border-dark mt-10 p-20 shadow-2xl">
					<div className="grid grid-cols-2 gap-3">
						<div className="bg-[#1c1919] rounded-3xl  shadow-[-1px_0px_2px_0px_#413F3F] p-5 flex flex-col items-center">
							<HiOutlineSquare3Stack3D size="2.5rem" className="text-yellow" />
							<div className="flex mt-6">
								<Image
									src={"/assets/images/ramos-person1.png"}
									width={50}
									height={50}
									alt="ramos:person1"
									className="rounded-full"
								/>
								<Image
									src={"/assets/images/ramos-person2.png"}
									width={50}
									height={45}
									alt="ramos:person2"
									className="rounded-full -ml-1 border-2 border-[#131111]"
								/>
							</div>
						</div>
						<Animate
							innerClassName="h-full"
							delay={0.5}
							animate={{
								hidden: {
									x: -20,
								},
								visible: {
									x: 0,
									transition: {
										duration: 0.8,
										ease: "easeInOut",
									},
								},
							}}
						>
							<div className="bg-[#1c1919] rounded-3xl  shadow-[-1px_0px_2px_0px_#413F3F] p-5 h-full flex flex-col">
								<p className="text-white mb-5 text-xs">Transactions</p>
								<sup className="text-green text-[0.5rem] text-right -mb-3">
									<BsFillArrowUpCircleFill
										size="1rem"
										className="inline mr-1 bg-white rounded-full"
									/>
									+14%
								</sup>
								<h2 className="text-6xl font-normal mt-4 text-white">
									<AnimateCounter value={43} />K
								</h2>
							</div>
						</Animate>
					</div>
					<div className="mt-10 text-center">
						<h4 className="font-medium text-white text-2xl mb-5">
							Widget control
						</h4>

						<Animate
							className=""
							delay={0.5}
							animate={{
								hidden: {
									opacity: 0.1,
									y: 30,
								},
								visible: {
									opacity: 1,
									y: 0,
									transition: {
										type: "spring",
										duration: 1,
										ease: "linear",
									},
								},
							}}
						>
							<p className="text-white/70 font-light text-lg">
								Reports provide a comprehensive overview of important aspects of
								web analytics
							</p>
						</Animate>
					</div>
				</div>
			</div>
			<div className="w-[66%] mx-auto flex flex-col md:flex-row mt-16  items-center">
				<p className="text-3xl font-medium mr-2 relative -bottom-8 text-nowrap">
					Up to
				</p>
				<h1 className="text-[8rem] font-medium mr-4">45%</h1>
				<Animate
					delay={0.5}
					animate={{
						hidden: {
							opacity: 0.1,
							y: 30,
						},
						visible: {
							opacity: 1,
							y: 0,
							transition: {
								type: "spring",
								duration: 1,
								ease: "linear",
							},
						},
					}}
					className="ml-6"
				>
					<p className="flex-1 text-lg">
						Increase your analytics efficiency by up to 45%. Unique algorithms
						provide insights from data, reduce time for analysis and save time
						for making important, informed decisions.
					</p>
				</Animate>
			</div>
		</Section>
	);
};

export default Analytics;
