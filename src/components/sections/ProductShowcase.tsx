import { useInView } from "react-intersection-observer";
import { useEffect, useState } from "react";
import { Section } from "../shared/Section";
import { Animate } from "../shared/animations";
import { ProductFeatures } from "@/constants";
import { HiOutlinePlusSm } from "react-icons/hi";
import Image from "next/image";
import { motion, useAnimation } from "framer-motion";

const ProductShowcase = () => {
	const [slideUpClass, setSlideUpClass] = useState("");
	const [ref, inView] = useInView({
		triggerOnce: true,
		threshold: 0.5,
	});
	
	const [refStats, inViewStats] = useInView({
		triggerOnce: true,
		threshold: 0.5,
	});

	const animation = useAnimation();

	useEffect(() => {
		if (inView) {
			setTimeout(() => {
				animation.start(
					{
						opacity: [0, 1, 1, 1, 1],
						borderBottomLeftRadius: ["0%", "100%", "10%", "0px", "0px"],
						borderBottomRightRadius: ["0%", "10%", "100%", "0px", "0px"],
						borderTopRightRadius: ["0%", "10%", "50%", "100%", "0%"],
						width: ["0%", "10%", "50%", "100%", "100%"],
						height: ["0%", "10%", "50%", "100%", "100%"],
					},
					{ duration: 2 }
				);
			}, 500);

			return () => animation && animation.stop();
		} else {
			animation.start("hidden");
		}
	}, [inView, animation]);

	useEffect(() => {
		if (inViewStats) {
			setSlideUpClass("slide-up");
		} else {
			setSlideUpClass("");
		}
	}, [inViewStats]);

	return (
		<main className="bg-light rounded-t-[100px] rounded-b-[100px] pb-24 relative max-w-[1400px] mx-auto">
			<Section className="py-32 lg:pr-0 z-10 relative">
				<div className="flex overflow-hidden">
					<div className="flex flex-col w-[50%]">
						<Animate
							className="overflow-hidden py-2"
							once={false}
							animate={{
								hidden: {
									y: 100,
									opacity: 0,
								},
								visible: {
									y: 0,
									opacity: 1,
									transition: {
										duration: 0.5,
										ease: "easeInOut",
									},
								},
							}}
						>
							<h3 className="font-medium text-6xl text-balance mb-20">
								Turning data into real actions and ideas.
							</h3>
						</Animate>
						{ProductFeatures.map((item, index) => (
							<Animate
								key={item}
								once={false}
								animate={{
									hidden: {
										y: index === 0 ? 100 : 50,
									},
									visible: {
										y: 0,
										transition: {
											duration: index === 2 ? 0 : index === 1 ? 0.6 : 0.7,
											ease: "easeInOut",
										},
									},
								}}
							>
								<div className="bg-white rounded-[1.9rem] px-6 py-4 mt-6 flex justify-between items-center w-[76%] shadow-xl">
									<h4 className="text-xl font-medium">{item}</h4>
									<div className="flex items-center justify-center w-14 h-14 bg-lightTertiary rounded-full">
										<HiOutlinePlusSm size={20} className="text-dark" />
									</div>
								</div>
							</Animate>
						))}
					</div>
					<div
						ref={refStats}
						className="w-[65%] flex relative justify-end h-[780px] -right-48"
					>
						<Image
							src="/assets/images/ramos-iphone-stats-empty.png"
							alt="ramos:iphone stats empty"
							width={300}
							height={900}
							className="relative -bottom-36 left-32 h-fit z-20"
						/>
						<Animate
							className="absolute -left-[128px] bottom-[1.2rem] z-20"
							once={false}
							animate={{
								hidden: {
									y: 100,
								},
								visible: {
									y: 0,
									opacity: 1,
									transition: {
										duration: 0.5,
										ease: "easeInOut",
									},
								},
							}}
						>
							<Image
								src="/assets/images/ramos-iphone-stats-shot.png"
								alt="ramos:iphone stats shot"
								width={278}
								height={365}
								className="h-fit z-20 "
							/>
						</Animate>
						<Image
							src="/assets/images/ramos-dashboard-stats-2.png"
							alt="ramos: dashboard stats"
							width={1000}
							height={900}
							className={`h-fit max-h-[650px] ${
								slideUpClass === "" ? "opacity-0" : slideUpClass
							}`}
						/>
					</div>
				</div>
			</Section>
			<Section>
				<div
					className="w-full h-[47%] absolute bottom-0 left-0 bg-[url('/assets/images/ramos-squares-fig.png')] text-center px-10"
					ref={ref}
				>
					<motion.div
						ref={ref}
						animate={animation}
						className="text-nowrap text-left w-0 h-0 overflow-hidden  text-orange bg-orange bg-clip-text z-0 font-semibold text-[26rem] md:pr-12 relative"
					>
						Ramos
					</motion.div>
				</div>
			</Section>
		</main>
	);
};

export default ProductShowcase;
