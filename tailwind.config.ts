import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        orange: "#FE4A23",
        yellow: "#FFD027",
        dark: "#0D0D0D",
        light: "#f9f9f9",
        lightSecondary: '#f0f0f0',
        lightTertiary: '#f2f2f2',
        lightQuaternary: '#d2d2d2',
        green: "#47c884",
        links: 'rgb(37,37,37)'

      },
    },
  },
  plugins: [],
};
export default config;
