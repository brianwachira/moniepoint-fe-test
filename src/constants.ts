export const LinksPrimary = [
    {
        name: 'Dashboard',
        path: '#'
    },
    {
        name: 'Reports',
        path: '#'
    },
    {
        name: 'Documents',
        path: '#'
    },
    {
        name: 'History',
        path: '#'
    },
    {
        name: 'Settings',
        path: '#'
    },

]

export const LinksSecondary = [
    {
        name: "About",
        path: '#'
    },
    {
        name: "Why Us",
        path: '#'
    },
    {
        name: "Platform",
        path: '#'
    },
    {
        name: "Pricing",
        path: '#'
    },
    {
        name: "Contacts",
        path: '#'
    }]

export const LinksContact = {
    social: [
        { name: "Linkedin", link: "#" }, { name: "Instagram", link: "#" }, { name: "Facebook", link: "#" }
    ],
    email: {
        address: "hello@ramos.com",

    },
    locations: [
        {
            region: "Raleigh",
            address: "125 N. Harrington StreetRaleigh,",
            zip: "NC 27603919.833.6413"
        },
        {
            region: "Charlotte",
            address: "220 East Peterson DriveCharlotte",
            zip: "NC 28217704.333.7272"
        }

    ]
}

export const dummyData = {
    items: [
        {
            title: 'Min. price',
            value: '1,200 $',
            image: '/assets/images/ramos-person1.png'
        },
        {
            title: 'Max. price',
            value: '2,320 $',
            image: '/assets/images/ramos-person2.png'
        },
    ],

}

export const ProductFeatures = [
    
        "Instant Insights", "AI Technology", "Easy Intergration"
]