import Layout from "@/components/Layout";
import AnalyticsOverview from "@/components/sections/AnalyticsOverview";
import CallToActionPrimary from "@/components/sections/CallToActionPrimary";
import CallToActionSecondary from "@/components/sections/CallToActionSecondary";
import DataOverview from "@/components/sections/DataOverview";
import Hero from "@/components/sections/Hero";
import ProductShowcase from "@/components/sections/ProductShowcase";
import { AnimatePresence } from "framer-motion";

export default function Home() {
	return (
    <AnimatePresence mode="sync" initial={true}>
      <Layout>
        <Hero/>
        <AnalyticsOverview/>
        <CallToActionPrimary/>
        <ProductShowcase/>
        <DataOverview/>
        <CallToActionSecondary/>
      </Layout>
    </AnimatePresence>
	);
}
