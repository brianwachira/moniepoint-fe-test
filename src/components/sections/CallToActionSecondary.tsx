import Link from "next/link";
import { Section } from "../shared/Section";
import { Animate } from "../shared/animations";
import { AnimateText } from "../shared/animations/AnimateText";

const CallToActionSecondary = () => {
	return (
		<Section className="pt-10 pb-36 flex flex-col items-center">
			<Animate
				once={false}
				animate={{
					hidden: {
						scale: 2,
						opacity: 0,
					},
					visible: {
						scale: 1,
						opacity: 1,
						transition: {
							duration: 1.5,
							ease: "easeInOut",
							type: "spring",
							bounce: 0.2,
						},
					},
				}}
			>
				<div className="bg-orange btn-shadow-orange rounded-[2rem] flex justify-center items-center w-[8rem] h-[8rem]">
					<img src="/assets/images/ramos-icon-orange.png" className=" w-12 h-12" alt="ramos:icon-orange"/>
				</div>
			</Animate>
			<div className="max-w-2xl text-center">
				<AnimateText
					className="text-[8rem] font-medium mt-3 text-nowrap"
					variant="statement"
					text="Get Started"
					animate={{
						hidden: {
							y: 50,
							opacity: 0,
						},
						visible: {
							y: 0,
							opacity: 1,
							transition: {
								duration: 0.7,
								ease: "easeInOut",
								type: "spring",
								bounce: 0.1,
								staggerChildren: 0.2,
							},
						},
					}}
				/>
				<div className="max-w-[29rem] mx-auto">
                <p className="text-2xl text-gray-400 ">Turn information into advantage! Start using Ramos today. Sign up for a free trial.</p>

				</div>
				<Animate
					once={false}
					animate={{
						hidden: {
							scale: 0.3,
							opacity: 0,
						},
						visible: {
							scale: 1,
							opacity: 1,
							transition: {
								duration: 0.5,
								ease: "easeInOut",
							},
						},
					}}
				>
					<div className="flex gap-4 mt-12 justify-center">
						<Link
							href={"#"}
							className="inline-block rounded-2xl px-12 py-4 text-sm font-medium  bg-lightTertiary text-dark"
						>
							Request a demo
						</Link>
						<Link
							href={"#"}
							className=" inline-block rounded-2xl px-12 py-4 text-sm font-medium  bg-orange text-white"
						>
							Start for free
						</Link>
					</div>
				</Animate>
			</div>
		</Section>
	);
};

export default CallToActionSecondary