"use client";
import { Variant, useAnimation, motion, useInView } from "framer-motion";
import React, { useEffect, useRef } from "react";

interface AnimateTextProps {
	text: string;
	className?: string;
	animate?: {
		hidden: Variant;
		visible: Variant;
	};
	viewDelay?: number;
	styleIndex?: {
		className: string;
		index: number;
	};
	variant?: "sentence" | "statement";
}
const animationsDefault = {
	hidden: {
		y: 100,
		opacity: 0,
	},
	visible: {
		y: 0,
		opacity: 1,
		transition: {
			duration: 0.5,
			ease: "easeIn",
			type: "spring",
			restSpeed: 0.2,
			mass: 5,
			damping: 40,
			velocity: 5,
		},
	},
};

export const AnimateText = ({
	text,
	className,
	animate = animationsDefault,
	viewDelay = 0,
	styleIndex,
	variant = "sentence",
}: AnimateTextProps) => {
	const controls = useAnimation();
	const ref = useRef(null);
	const isInView = useInView(ref, { amount: viewDelay, once: true });
	const textArray = Array.isArray(text) ? text : [text];

	useEffect(() => {
		if (isInView) {
			controls.start("visible");
		} else {
			controls.start("hidden");
		}
	}, [isInView]);

	if (variant === "statement") {
		return (
			<>
				<motion.span
					ref={ref}
					initial="hidden"
					animate={controls}
					variants={{
						visible: { transition: { staggerChildren: 0.1 } },
						hidden: {},
					}}
					aria-hidden
					className={className}
					style={{
						display: "inline-block",
						overflowY: "hidden",
						marginBottom: "-0.15em",
					}}
				>
					{textArray.map((item, lineIndex) => (
						<span className="" key={`${item}-${lineIndex}`}>
							{item.split(" ").map((word: string, wordIndex: any) => (
								<motion.span
									className={`inline-block ${
										styleIndex?.index === wordIndex ? styleIndex?.className : ""
									}`}
									key={`${word}-${wordIndex}`}
								>
									{word.split("").map((char, charIndex) => (
										<motion.span
											key={`${char}-${charIndex}`}
											className="inline-block"
											variants={animate}
										>
											{char}
										</motion.span>
									))}
									&nbsp;
								</motion.span>
							))}
						</span>
					))}
				</motion.span>
			</>
		);
	}
	return (
		<>
			<motion.span
				ref={ref}
				initial="hidden"
				animate={controls}
				variants={{
					visible: { transition: { staggerChildren: 0.1 } },
					hidden: {},
				}}
				aria-hidden
				className={className}
				style={{
					display: "inline-block",
					overflowY: "hidden",
					marginBottom: "-0.15em",
				}}
			>
				{textArray.map((item, lineIndex) => (
					<span className="" key={`${item}-${lineIndex}`}>
						{item.split(" ").map((word: string, wordIndex: any) => (
							<span
								className={`inline-block ${
									styleIndex?.index === wordIndex ? styleIndex?.className : ""
								}`}
								key={`${word}-${wordIndex}`}
							>
								{word.split("").map((char, charIndex) => (
									<motion.span
										key={`${char}-${charIndex}`}
										className="inline-block"
										variants={animate}
									>
										{char}
									</motion.span>
								))}
								&nbsp;
							</span>
						))}
					</span>
				))}
			</motion.span>
		</>
	);
};
