import { LiaLinkSolid } from "react-icons/lia";
import { Animate } from "../shared/animations";
import { LinksPrimary } from "@/constants";
import Link from "next/link";
import Image from "next/image";

const Header = () => {
	return (
		<nav className="flex justify-between bg-dark text-white rounded-2xl py-2 pl-3 pr-2 mt-5 overflow-hidden mx-4">
			<Animate
				className="flex"
				innerClassName="flex items-center"
				delay={0}
				once={true}
				animate={{
					hidden: {
						y: 50,
					},
					visible: {
						y: 0,
						transition: {
							duration: 1.5,
							ease: "easeInOut",
							type: "spring",
							bounce: 0.2,
						},
					},
				}}
			>
				{/* <div className="flex items-center">
					<LiaLinkSolid size={18} className="font-bold" />
					<h4 className="font-semibold ml-1 text-lg">ramos</h4>
				</div> */}

				<img alt='ramos:logo' src='/assets/icons/ramos-logo.png' className="h-6"/>
			</Animate>
			<Animate
				delay={0}
				once={true}
				animate={{
					hidden: {
						y: 50,
					},
					visible: {
						y: 0,
						transition: {
							duration: 1.5,
							ease: "easeInOut",
							type: "spring",
							bounce: 0.2,
						},
					},
				}}
			>
				<div className="bg-links rounded-2xl px-4 py-4">
					<ul className="flex items-center space-x-7">
						{LinksPrimary.map((item, index) => (
							<li key={index} className="">
								<Link
									href={item.path}
									className="text-sm hover:text-primary p-1 transition-all duration-300"
								>
									{item.name}
								</Link>
							</li>
						))}
					</ul>
				</div>
			</Animate>
			<Animate
				delay={0}
				once={true}
				animate={{
					hidden: {
						y: 50,
					},
					visible: {
						y: 0,
						transition: {
							duration: 1.5,
							ease: "easeInOut",
							type: "spring",
							bounce: 0.2,
						},
					},
				}}
			>
				<Link
					href={"#"}
					className="inline-block rounded-2xl px-8 py-4 text-sm font-normal bg-white text-dark"
				>
					Sign up
				</Link>
			</Animate>
		</nav>
	);
};

export default Header;
