import { FaPlay } from "react-icons/fa";
import { Section } from "../shared/Section";
import { Animate } from "../shared/animations";
import Image from "next/image";
import { PiLightningFill } from "react-icons/pi";
import { IoAnalyticsSharp } from "react-icons/io5";
import { AnimateText } from "../shared/animations/AnimateText";
import { FiBarChart2 } from "react-icons/fi";

const Hero = () => {
	return (
		<Section className="h-[calc(100vh_-_72px)] lg:px-9 table">
			<div className="mx-auto relative table-cell align-middle ">
				<div className="float-right ml-5 relative">
					<div className="w-[3rem] h-[3rem] flex justify-center items-center bg-orange rounded-full absolute -top-3 -left-3 shadow-lg z-10">
						<FaPlay size="1.1rem" className="fill-white" />
					</div>
					<Animate
						delay={0}
						animate={{
							hidden: {
								scale: 0.2,
							},
							visible: {
								scale: 1,
								transition: {
									duration: 1,
									ease: "easeInOut",
								},
							},
						}}
					>
						<Image
							src="/assets/images/ramos-hero-image.png"
							width={400}
							height={400}
							alt="ramos:hero image"
							className=""
							sizes="(min-width: 768px) 100vw, 500px"
						/>
					</Animate>
				</div>
				<div className="relative">
					<div className="h-[7rem] flex items-center mt-5 absolute top-0 left-36">
						<Animate
							animate={{
								hidden: {
									scale: 0.2,
									rotate: 180,
								},
								visible: {
									scale: 1,
									rotate: 0,
									transition: {
										duration: 1,
										ease: "easeInOut",
									},
								},
							}}
						>
							<div className=" w-28 h-28 rounded-full bg-lightTertiary flex items-center ">
								<PiLightningFill
									size="2.5rem"
									className="text-orange mx-auto"
								/>
							</div>
						</Animate>
						<Animate
							animate={{
								hidden: {
									scale: 0.2,
									rotate: 180,
								},
								visible: {
									scale: 1,
									rotate: 0,
									transition: {
										duration: 1,
										ease: "easeInOut",
									},
								},
							}}
						>
							<div className="w-28 h-28 rounded-full bg-orange -ml-4 flex items-center ">
								<IoAnalyticsSharp
									size="2.5rem"
									className="text-lightTertiary  mx-auto "
								/>
							</div>
						</Animate>
					</div>
					<h1 className="text-[8rem] leading-[1.1] font-medium mt-5 text-right">
						<AnimateText text="Analytics" />
					</h1>
					<h1 className="text-[8rem] leading-[1.1] font-medium ml-10  text-left">
						<AnimateText
							text="that helps you"
							styleIndex={{
								index: 1,
								className: "text-lightQuaternary",
							}}
						/>
					</h1>
					<h1 className="text-[8rem] leading-[1.1] font-medium -mr-8 text-right">
						<div>
							<AnimateText text="shape" />
							<Animate
								className="inline-flex relative -bottom-2"
								animate={{
									hidden: {
										scale: 0.1,
									},
									visible: {
										scale: 1,
										transition: {
											duration: 1,
											ease: "easeInOut",
											type: "spring",
										},
									},
								}}
							>
								<FiBarChart2
									size="7rem"
									className="text-dark rounded-full p-5 bg-yellow inline rotate-[360deg]"
								/>
							</Animate>
							&nbsp;
							<AnimateText
								text="the future"
								animate={{
									hidden: {
										y: 200,
									},
									visible: {
										y: 0,
										transition: {
											duration: 1,
											ease: "easeInOut",
										},
									},
								}}
							/>
						</div>
					</h1>
				</div>
			</div>
		</Section>
	);
};

export default Hero;
