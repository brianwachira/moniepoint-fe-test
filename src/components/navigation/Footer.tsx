import { LinksContact, LinksSecondary } from "@/constants";
import { Section } from "../shared/Section";
import { Animate } from "../shared/animations";
import Link from "next/link";
import { AnimateText } from "../shared/animations/AnimateText";
import Image from "next/image";

const Footer = () => {
	return (
		<div className="bg-dark text-white">
			<Section className="pt-[7.5rem] pb-20">
				<div className="flex justify-between items-center">
					<div className="flex gap-5">
						{LinksSecondary.map((item) => (
							<Link
								key={item.name}
								href={item.path}
								className="text-light/50 text-xl"
							>
								{item.name}
							</Link>
						))}
					</div>
					<Animate
						once={false}
						animate={{
							hidden: {
								y: 30,
							},
							visible: {
								y: 0,
								transition: {
									duration: 1.5,
									ease: "easeInOut",
									type: "spring",
									bounce: 0.2,
								},
							},
						}}
					>
						<Link
							href={`mailto:${LinksContact.email.address}`}
							className="font-medium text-6xl"
						>
							{LinksContact.email.address}
						</Link>
					</Animate>
				</div>
                <hr className="border-t border-light/50 mt-12 pt-12" />
				<div className=" flex justify-between mb-10">
					<div className="flex w-[45%] justify-between">
						{LinksContact.locations.map((item) => (
							<Animate
								className=""
								key={item.region}
								once={false}
								animate={{
									hidden: {
										y: 30,
									},
									visible: {
										y: 0,
										transition: {
											duration: 1.5,
											ease: "easeInOut",
											type: "spring",
											bounce: 0.2,
										},
									},
								}}
							>
								<div className="flex flex-col gap-1">
									<h5 className="text-lg font-medium">{item.region}</h5>
									<p className="text-light/50 text-lg">{item.address}</p>
									<p className="text-light/50 text-lg">{item.zip}</p>
								</div>
							</Animate>
						))}
					</div>
					<div className="flex flex-col gap-5">
						{LinksContact.social.map((item) => (
							<Link
								key={item.name}
								href={item.link}
								className="text-light text-2xl text-right"
							>
								{item.name}
							</Link>
						))}
					</div>
				</div>
				<div className=" flex justify-between gap-20">
					<h1 className="text-[11rem] leading-[1.1] font-medium text-nowrap">
						<AnimateText
							text="Ramos"
							animate={{
								hidden: {
									y: 50,
									opacity: 0,
								},
								visible: {
									y: 0,
									opacity: 1,
									transition: {
										duration: 0.3,
										ease: "easeInOut",
									},
								},
							}}
						/>
						<sup className="text-3xl align-super -ml-7">&#9415;</sup>
					</h1>
					<div className="flex items-center w-1/2 justify-between">
						<Link href="#" className="text-light/50 text-base">
							Privacy policy
						</Link>
						<Link href="#" className="text-light/50 text-base">
							License agreement
						</Link>
						<Animate
							className=""
							once={false}
							animate={{
								hidden: {
									scale: 0.2,
								},
								visible: {
									scale: 1,
									transition: {
										duration: 1,
										ease: "easeInOut",
										type: "spring",
										bounce: 0.2,
									},
								},
							}}
						>
							<div className="bg-white rounded-3xl w-[150px] h-[150px] flex items-center justify-center">
								<Image
									src="/assets/images/ramos-qr-code.png"
									width={150}
									height={150}
									alt="ramos qr code"
								/>
							</div>
						</Animate>
					</div>
				</div>
			</Section>
		</div>
	);
};

export default Footer;
